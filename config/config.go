/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
    "gitlab.com/whom/shs/log"
    "gitlab.com/whom/shs/ast"
    "gitlab.com/whom/shs/util"
    "gitlab.com/whom/shs/stdlib"
)

/* creates new VarTable and FuncTable
 * reads a configuration file
 * executes contents and returns tables
 */
func InitFromConfig(configFile string) (ast.VarTable, ast.FuncTable) {
    funcs := stdlib.GenFuncTable()
    vars := &map[string]*ast.Token{}

    ast.InitVarTable(vars)

    p := ast.GetVar("HOME", vars)
    configFile = p.Value() + "/" + configFile

    util.LoadScript(configFile, vars, funcs)

    log.Log(log.INFO,
        "config file fully evaluated",
        "config")
    return vars, funcs
}

/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ast

/* primitive stack type for tokens
 * useful for iterative algorithms on tokens
 */
type TokenStack struct {
    buffer []*Token
    capacity int
}

/* push token onto stack
 */
func (s *TokenStack) Push(v *Token) {
    s.capacity++
    s.buffer = append(s.buffer, v)
}

/* pop token off stack
 */
func (s *TokenStack) Pop() *Token {
    if s.capacity <= 0 {
        return nil
    }

    s.capacity--
    res := s.buffer[len(s.buffer) - 1]
    s.buffer = s.buffer[:len(s.buffer) - 1]
    return res
}

/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ast

import (
    "fmt"
    "strings"
)

/* Print function which breaks each embedded list out on individual lines.
 * Used in the print_ast debug tool. not too useful for repl applications.
 * Very useful for debugging syntax though.
 * TODO: Add numbers to denote embedded scope?
 */
func PrintSExprsIndividually(arg *Token) {
    if arg == nil {
        return //TODO: Handle error here?
    }

    var lists TokenStack;
    lists.Push(arg)

loop:
    var constructor strings.Builder
    i := lists.Pop()
    if i == nil {
        return
    }

    for iter := i; iter != nil; iter = iter.Next {
        if iter.Tag == LIST {
            lists.Push(iter.Expand())
        }

        constructor.WriteString(iter.FmtToken())
    }

    fmt.Printf(constructor.String() + "\n")
    goto loop
}


/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ast

import (
    "gitlab.com/whom/shs/log"
)

/* expected function header for any stdlib function
 */
type Operation func(*Token, VarTable, FuncTable) *Token

/* holds a stdlib function along with relevant metadata
 */
type Function struct {
    // go function that list of args are passed to
    Function    Operation

    // name of function
    Name        string

    // number of times user has called this function
    TimesCalled int

    // list of types (LIST, NUMBER, STRING, etc) representing args
    Args        []Token_t
    NumArgs     int // -1 means infinite

    // lazy arg checking (use NumArgs instead of args)
    ArgLazy     bool

    // dont fail on undefined symbol (passed to eval when parsing args)
    SymLazy   bool

    // dont eval args at all, leave that to the function
    EvalLazy    bool
}

/* holds a mapping of key to function
 * passed to eval and into function calls
 * initialized by repl at startup
 */
type FuncTable *map[string]*Function

/* validates an individual call of a function
 * makes sure correct arguments are passed in
 */
func (f Function) ParseFunction(args *Token) bool {
    inc := 0
    for iter := args; iter != nil; iter = iter.Next {
        inc += 1
        if inc > len(f.Args) {
            log.Log(log.ERR,
                "too many arguments",
                "ftable")
            return false
        }

        if iter.Tag != f.Args[inc - 1] {
            log.Log(log.ERR,
                "argument is " + GetTagAsStr(iter.Tag) +
                " should be " + GetTagAsStr(f.Args[inc - 1]),
                "ftable")
            return false
        }
    }

    if inc < len(f.Args) {
        log.Log(log.ERR,
            "not enough args given",
            "ftable")
        return false
    }

    return true
}

/* same as ParseFunction but only evaluates the number of args
 */
func (f Function) LazyParseFunction(args *Token) bool {
    if f.NumArgs < 0 {
        return true
    }

    total := 0
    for iter := args; iter != nil; iter = iter.Next {
        total += 1
    }

    if total < f.NumArgs {
        log.Log(log.ERR,
            "expected more arguments, try calling info on function",
            "ftable")
        return false
    }

    if total > f.NumArgs {
        log.Log(log.ERR,
            "too many args. try calling info on function",
            "ftable")
        return false
    }

    return true
}

/* handles a call to a function
 * calls ParseFunction and increments TimesCalled
 */
func (f Function) CallFunction(args *Token, vt VarTable, ft FuncTable) *Token {
    n_args := args
    if !f.EvalLazy {
        n_args = args.Eval(ft, vt, f.SymLazy)
    }

    passes := false
    if f.ArgLazy {
        passes = f.LazyParseFunction(n_args)
    } else {
        passes = f.ParseFunction(n_args)
    }

    if !passes {
        log.Log(log.ERR,
                "Couldnt call " + f.Name,
                "eval")
        return nil
    }

    f.TimesCalled += 1
    return f.Function(n_args, vt, ft)
}

/* searches for function mapped to argument in FuncTable
 */
func GetFunction(arg string, table FuncTable) *Function {
    target, ok := (*table)[arg]
    if !ok {
        log.Log(log.INFO,
                "function " + arg + " not found",
                "ftable")
        return nil
    }

    return target
}


/* returns list of all functions in table
 */
func ListFuncs(ft FuncTable) []string {
    keys := make([]string, len(*ft))
    i := 0

    for k := range *ft {
        keys[i] = k
        i++
    }

    return keys
}

/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ast

import (
    "gitlab.com/whom/shs/log"
)

/* determines whether or not to execute a system binary
 * when a function cannot be found in the functable
 * (use case: shell)
 */
var ExecWhenFuncUndef = false

/* name of the command used to execute a system binary
 */
var ExecFunc = "l"

/* Runs through an AST of tokens
 * Evaluates the Tokens to determine simplest form
 * Returns simplest form
 *
 * canFunc determines whether a symbol could be a function to call
 * (true when first elem of a list)
 */
func (in *Token) Eval(funcs FuncTable, vars VarTable, cnvtUndefVars bool) *Token {
    if in == nil {
        return nil
    }

    var res *Token

    switch in.Tag {
    case BOOL, NUMBER, STRING:
        res = in.Copy()

    case SYMBOL:
        res = GetVar(in.Value(), vars)
        if res == nil {
            res = in.Copy()

            if GetFunction(in.Value(), funcs) == nil {
                if cnvtUndefVars {
                    res.Tag = STRING
                    break
                }

                log.Log(log.ERR,
                    "undefined symbol: "+in.Value(),
                    "eval")
                return nil
            }
        } else {
            res.Next = in.Next
        }

    case LIST:
        inner := in.Expand()
        if inner == nil {
            res = in.Copy()
            break
        }

        if inner.Tag != SYMBOL {
            in.Direct(inner.Eval(funcs, vars, cnvtUndefVars))
            res = in.Copy()
            break
        }

        makeHead := false
        funct := GetFunction(inner.Value(), funcs)
        if funct == nil {
            if ExecWhenFuncUndef {
                funct = GetFunction(ExecFunc, funcs)
                makeHead = true
            }
        }

        if funct != nil {
            if makeHead {
                inner = &Token{Next: inner}
            }
            res = funct.CallFunction(inner.Next, vars, funcs).Eval(funcs, vars, false)
            if res == nil {
                // function failed. logging is its responsibility
                return nil
            }

            res.Append(in.Next)

        } else {
            log.Log(log.ERR,
                "undefined function "+inner.Value()+" called",
                "eval")
            return nil
        }

    default:
        log.Log(log.ERR,
            "Eval hit unknown token type!",
            "eval")
        return nil
    }

    if res.Next != nil {
        res.Next = res.Next.Eval(funcs, vars, cnvtUndefVars)
    }
    return res
}

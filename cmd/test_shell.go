/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
    "fmt"
    "time"
    "gitlab.com/whom/shs/log"
    "gitlab.com/whom/shs/stdlib"
)

func main() {
    log.SetLogLvl(3)
    stdlib.InitShellFeatures()
    stdlib.LaunchProcess("/usr/bin/vim", []string{"test.txt"}, false)

    fmt.Println("process started, now sleeping for 10 sec")
    time.Sleep(10 * time.Second)

    stdlib.TeardownShell()
}

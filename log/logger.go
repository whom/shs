/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package log

import (
    "fmt"
)

const (
    ERR     int = 0
    DEBUG   int = 1
    INFO    int = 2
)

var logLevel int64

/* Set the log level from 0 to 4
 * currently only 0, 1, and 2 are used.
 */
func SetLogLvl(lvl int64) {
    if lvl < 4 && lvl > 0 {
        logLevel = lvl
    }
}

/* get current log level as int
 */
func GetLogLvl() int64 {
    return logLevel
}

/* writes a message to the log
 */
func Log(lvl int, msg, context string) {
    if int64(lvl) > logLevel {
        return
    }

    fmt.Println("[" + context + "]\033[3m " + msg + "\033[0m ")
}

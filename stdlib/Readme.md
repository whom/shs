# SHS STDLIB

In here are definitions for builtin functions that can be used from the repl. If you are looking for a way to extend this shell to fit your use case you are in the right spot.

## Datatypes

Within the AST package you can see [a number of important definitions.]("https://git.callpipe.com/aidan/shs/-/blob/master/ast/func_table.go") Worth noting are FuncTable, Operation, and Function.

```go
type Operation func(*Token, VarTable, FuncTable) *Token

type Function struct {
    function    Operation
    name        string
    timesCalled int
    args        int
}

type FuncTable map[string]*Function
```

## The Standard Library

The standard library is loaded during the init step of the repl (or interpreter if you have embedded one). Alternatively, it can be loaded into your application by importing `git.callpipe.com/shs/stdlib`. In order to get a table of all available functions the `GenFuncTable` function is called, returning a struct of type `ast.FuncTable`. Any functions in the standard lib must be accounted for here in order for them to be available at repl or interpreter. Each Operation operates on a list of [Tokens](https://git.callpipe.com/aidan/shs/-/blob/master/ast/token.go). Every function should adhere to the idea that it takes in a list of Tokens and returns a list of Tokens.

## Working with Tokens
[Tokens](https://git.callpipe.com/aidan/shs/-/blob/master/ast/token.go) are a rudimentary linked list of parsed [Lexemes](https://en.wikipedia.org/wiki/Lexeme). In the ast package there are definitions for Tokens, as well as code for the combined Lex/Parse loop that creates them. Tokens are built in a way that makes operating over them with either recursive or iterative alrogithms easy. When consuming Tokens, one can expect their type by looking at the Tag field. The data stored in the Inner field will be either a string or a \*Token depending on what Tag is. You can expect a \*Token if the Tag field is ast.LIST, and a string in all other cases. If the Tag field is ast.SYMBOL you can look it up in the VarTable or the FuncTable. The VarTable will return either a \*Token (if the symbol is a Variable) or *nil* if nothing is found. The FuncTable will return either a \*Function (if there is a match) or it will return *nil*.
P.S.: Ideally a token should not be re-used. You may consider them disposable. It is up to you to make sure that any Token you edit/reuse remains consistant with the type declared in its TAG. Make sure to differentiate between NUMBER and STRING with the `ast.StrIsNumber(arg string) bool` function.
## Adding a function
There are two ways to define functions: Either by writing it in shs code (using the 'func' function) or by extending the standard library. The steps below assume you are extending the standard library.
1. *Write your function in the form of an `ast.Operation`.* Any function that has the defined signature can be an Operation.
2. *Create a `Function` to encapsulate your `Operation`.* Make sure to set the `args` and `name` fields. Args will be used to validate function calls and Name will be used in debug/log output.
3. *Add your `Function` to the `FuncTable`.* Make sure your `Operations`s get added to the table generated in `GenFuncTable`.

## Tips
- You can use the Log module to add helpful output to your functions.
- Try not to clutter the output with unnessesary ERR level logging.
- Make sure you properly set the Next element of any Token you swap into a list
- Make sure you properly set the Next element of the previous Token as you swap a token into a list

## License
Copyright (C) 2019 Aidan Hahn.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/

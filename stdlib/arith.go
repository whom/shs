/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package stdlib

import (
    "fmt"
    "strconv"
    "gitlab.com/whom/shs/ast"
    "gitlab.com/whom/shs/log"
)

// PKG WIDE TODO: BIGNUM SYSTEM
// then write actually optimal routines once it is in place
// perhaps we simply write out arithmetic routines that operate on the strings
// then we need not worry about storage length.

/* Takes 1 argument (must be a string)
 * will attempt to cast it to a number.
 * will return nil if cast fails
 *
 * Example: (number "3.4")
 */
func NumCast(in *ast.Token, a ast.VarTable, f ast.FuncTable) *ast.Token {
    if !ast.StrIsNumber(in.Value()) {
        log.Log(log.ERR,
            "string failed number cast",
            "number_cast")
        return nil
    }

    out := in.Copy()
    out.Tag = ast.NUMBER
    return out
}

/* adds N number arguments
 * takes N arguments
 * returns the sum, or nil if improper arguments were given
 *
 * Example: (+ 1 2)
 */
func Add(in *ast.Token, a ast.VarTable, f ast.FuncTable) *ast.Token {
    var res float64
    for i := in; i != nil; i = i.Next {
        if i.Tag != ast.NUMBER {
            log.Log(log.ERR, "Non-number given to ADD", "add")
            return nil
        }

        token := i.Value()
        isFloat := false
        for _, char := range token {
            if char == '.' {
                isFloat = true
                n, err := strconv.ParseFloat(token, 64)
                if err != nil {
                    log.Log(log.ERR, "Err parsing number: " + err.Error(), "add")
                    return nil
                }

                res += n
            }
        }

        if !isFloat {
            n, err := strconv.ParseInt(token, 10, 64)
            if err != nil {
                log.Log(log.ERR, "Err parsing number: " + err.Error(), "add")
                return nil
            }

            res += float64(n)
        }
    }

    t := &ast.Token{Tag: ast.NUMBER}
    t.Set(fmt.Sprintf("%f", res))
    return t
}

/* subtract N args from the final arg
 * takes N args, returns nil if improper args given
 *
 * Example: (- 2 1)
 */
func Sub(in *ast.Token, a ast.VarTable, f ast.FuncTable) *ast.Token {
    var res float64
    var sub float64
    for i := in; i != nil; i = i.Next {
        if i.Tag != ast.NUMBER {
            log.Log(log.ERR, "Non-number given to SUB", "sub")
            return nil
        }

        token := i.Value()
        isFloat := false
        var inner float64
        for _, char := range token {
            if char == '.' {
                isFloat = true
                n, err := strconv.ParseFloat(token, 64)
                if err != nil {
                    log.Log(log.ERR, "Err parsing number: " + err.Error(), "sub")
                    return nil
                }

                inner = n
            }
        }

        if !isFloat {
            n, err := strconv.ParseInt(token, 10, 64)
            if err != nil {
                log.Log(log.ERR, "Err parsing number: " + err.Error(), "sub")
                return nil
            }

            inner = float64(n)
        }

        if i.Next != nil {
            sub += inner
        } else {
            res = inner
        }
    }

    t := &ast.Token{Tag: ast.NUMBER}
    t.Set(fmt.Sprintf("%f", res - sub))
    return t
}

/* multiplies N arguments
 * returns nil if an improper argument is given
 *
 * Example: (* 1 2)
 */
func Mult(in *ast.Token, a ast.VarTable, f ast.FuncTable) *ast.Token {
    res := 1.0
    for i := in; i != nil; i = i.Next {
        if i.Tag != ast.NUMBER {
            log.Log(log.ERR, "Non-number given to MULT", "mult")
            return nil
        }

        token := i.Value()
        isFloat := false
        for _, char := range token {
            if char == '.' {
                isFloat = true
                n, err := strconv.ParseFloat(token, 64)
                if err != nil {
                    log.Log(log.ERR, "Err parsing number: " + err.Error(), "mult")
                    return nil
                }

                res *= n
            }
        }

        if !isFloat {
            n, err := strconv.ParseInt(token, 10, 64)
            if err != nil {
                log.Log(log.ERR, "Err parsing number: " + err.Error(), "mult")
                return nil
            }

            res *= float64(n)
        }
    }

    t := &ast.Token{Tag: ast.NUMBER}
    t.Set(fmt.Sprintf("%f", res))
    return t
}

/* divide N arguments
 * the first argument is divided by each subsequent argument in order
 * returns nil if an improper argument is given
 *
 * Example (/ 25 5)
 */
func Div(in *ast.Token, a ast.VarTable, f ast.FuncTable) *ast.Token {
    var res float64
    for i := in; i != nil; i = i.Next {
        inner := 0.0

        if i.Tag != ast.NUMBER {
            log.Log(log.ERR, "Non-number given to DIV", "div")
            return nil
        }

        token := i.Value()
        isFloat := false
        for _, char := range token {
            if char == '.' {
                isFloat = true
                n, err := strconv.ParseFloat(token, 64)
                if err != nil {
                    log.Log(log.ERR, "Err parsing number: " + err.Error(), "div")
                    return nil
                }

                inner = n
            }
        }

        if !isFloat {
            n, err := strconv.ParseInt(token, 10, 64)
            if err != nil {
                log.Log(log.ERR, "Err parsing number: " + err.Error(), "div")
                return nil
            }

            inner = float64(n)
        }

        if i == in {
            res = inner
        } else {
            res = res / inner
        }
    }

    t := &ast.Token{Tag: ast.NUMBER}
    t.Set(fmt.Sprintf("%f", res))
    return t
}

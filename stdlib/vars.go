/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package stdlib

import (
    "gitlab.com/whom/shs/ast"
    "gitlab.com/whom/shs/log"
)

/* Takes 2 args, a name and a value
 * Exports a varable
 * both args are evaluated
 *
 * Example:
 *    (export hw (concat "hello" " " "world"))
 *    (print hw)
 */
func Export(input *ast.Token, vars ast.VarTable, funcs ast.FuncTable) *ast.Token {
    name := input

    form := name.Next.Eval(funcs, vars, false)

    // error in eval process
    if form == nil {
        return nil
    }

    if name.Tag != ast.SYMBOL {
        log.Log(log.ERR,
            "first arg should be a symbol",
            "export")
        return nil
    }

    ast.SetVar(name.Value(), form, vars)
    return nil
}


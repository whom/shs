/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package stdlib

import (
    "fmt"
    "strings"
    "strconv"
    "gitlab.com/whom/shs/ast"
    "gitlab.com/whom/shs/log"
)

/* Concatenates N stringables
 *
 * Example: (concat "hello" " " "world")
 */
func Concat(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    var res string
    for i := in; i != nil; i = i.Next {
        if i.Tag == ast.LIST {
            log.Log(log.ERR, "Not concatenating list", "conc")
            log.Log(log.DEBUG, "Try using the expand operator (...)", "conc")
            continue
        }

        res += i.Value()
    }

    t := &ast.Token{Tag: ast.STRING}
    t.Set(res)
    return t
}

/* Takes 1 argument, returns its value as a string
 * works on lists too.
 *
 * Example: (string 1) -> 1.0
 */
func StrCast(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    body := in.String()
    res := &ast.Token{ Tag: ast.STRING }
    res.Set(body)
    return res
}

/* Takes 2 arguments, a string and a delimiter
 * returns a list of substrings found delimited by the delimiter from the parent string
 * Filters out all empty segments between delimiters
 *
 * Example: (split "/path/to/file" "/") -> ("path" "to" "file")
 */
func Split(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    body := in.Value()
    delim := in.Next.Value()

    var res *ast.Token
    builder := &res
    strtoks := strings.Split(body, delim)
    for _, i := range strtoks {
        if i == "" {
            continue
        }

        *builder = &ast.Token{Tag: ast.STRING}
        (*builder).Set(i)
        builder = &(*builder).Next
    }

    return res
}

/* Takes three args
 * 1. source string
 * 2. token to be replaced
 * 3. token to swap in place
 * All three args are strings
 * Returns final string
 */
func Replace(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    body := in.Value()
    sub := in.Next.Value()
    tok := in.Next.Next.Value()

    body = strings.ReplaceAll(body, sub, tok)

    res := &ast.Token{Tag: ast.STRING}
    res.Set(body)
    return res
}

/* Takes two args, a delimiter and a list of strings
 * Returns the list of strings concatenated together with the delimiter in between each element
 * On error returns nil
 *
 * Example: (join ", " ("apple" "ananas" "pear")) -> "apple, ananas, pear"
 */
func Join(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    delim := in
    strs := in.Next
    de := delim.Value()
    res := ""

    for i := strs.Expand(); i != nil; i = i.Next {
        if i.Tag != ast.STRING {
            log.Log(log.ERR,
                "all items to be joined must be strings",
                "join")
            return nil
        }

        res += i.Value()
        if i.Next != nil {
            res += de
        }
    }

    ret := &ast.Token{Tag: ast.STRING}
    ret.Set(res)
    return ret
}

/* takes three arguments:
 *   1. start index
 *   2. end index
 *   3. source
 * Returns a substring from source delimited by args 1 and 2.
 * First two args must be integers (4 or 4.0 but not 4.3)
 *
 * Example: (substr 1 5 "Linus Torvalds") -> "inus "
 */
func Substr(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    start := in
    end := start.Next
    str := end.Next

    ed_idx := 0
    st_idx, err := strconv.Atoi(start.Value())
    ed_idx, err = strconv.Atoi(end.Value())
    if err != nil {
        log.Log(log.ERR,
            "error parsing args: " + err.Error(),
            "substr")
        return nil
    }

    strlen := len(str.Value())
    if st_idx < 0 {
        st_idx += strlen
    }

    if ed_idx < 0 {
        ed_idx += strlen
    }

    if st_idx < 0 || st_idx >= strlen {
        log.Log(log.ERR,
            "first index out of bounds",
            "substr")
        return nil
    }

    if ed_idx < 0 || ed_idx >= strlen {
        log.Log(log.ERR,
            "last index out of bounds",
            "substr")
        return nil
    }

    if st_idx > ed_idx {
        log.Log(log.ERR,
            "start must be less than end",
            "substr")
        return nil
    }

    res := str.Value()[st_idx:ed_idx]
    ret := &ast.Token{Tag: ast.STRING}
    ret.Set(res)
    return ret
}

/* Takes one arg, returns nil
 * Prints a string to stdout
 * Unquotes string so user can add escaped chars like \n, \t, etc
 *
 * Example: (print "Line: \n, Tab: \t")
 */
func PrintStr(in *ast.Token, vt ast.VarTable, ft ast.FuncTable) *ast.Token {
    body := in.String()
    if body[0] != body[len(body)-1] && body[0] != '"' {
        body = "`" + body + "`"
    }

    text, err := strconv.Unquote(body)
    if err != nil {
        log.Log(log.ERR,
            "error unquoting string",
            "print")
        return nil
    }

    fmt.Printf(text + "\n")
    return nil
}

module gitlab.com/whom/shs

go 1.14

require (
	github.com/candid82/liner v1.4.0 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/peterh/liner v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666
)

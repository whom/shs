/*   SHS: Syntactically Homogeneous Shell
 *   Copyright (C) 2019  Aidan Hahn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package util

import (
    "os"
    "io/ioutil"
    "gitlab.com/whom/shs/log"
    "gitlab.com/whom/shs/ast"
)

/* Opens a file and lexes+evaluates the contents
 */
func LoadScript(path string, vt ast.VarTable, ft ast.FuncTable) {
    scriptFile, err := os.Open(path)
    if err != nil {
        log.Log(log.ERR,
            "unable to open script: " + err.Error(),
            "util")
        return
    }

    var body []byte
    body, err = ioutil.ReadFile(path)
    scriptFile.Close()
    if err != nil {
        log.Log(log.ERR,
            "unable to read script: " + err.Error(),
            "util")
        return
    }

    set := ast.Lex(string(body))
    for iter := set; iter != nil; iter = iter.Next {
        iter.Eval(ft, vt, false)
    }
}
